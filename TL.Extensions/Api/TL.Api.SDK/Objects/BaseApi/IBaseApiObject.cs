﻿using TL.Engine.SDK.Objects;

namespace TL.Api.SDK.Objects
{
    /// <summary>
    /// Интерфейс базового сериализованного объекта API документации
    /// </summary>
    /// <seealso cref="IBaseObject" />
    public interface IBaseApiObject : IBaseObject
    {
    }
}
