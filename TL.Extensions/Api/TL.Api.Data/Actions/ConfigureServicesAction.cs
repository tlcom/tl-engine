﻿using ExtCore.Infrastructure.Actions;
using Microsoft.Extensions.DependencyInjection;
using System;
using TL.Api.Data.Managers;

namespace TL.Api.Data.Actions
{
    public class ConfigureServicesAction : IConfigureServicesAction
    {
        public int Priority => 1000;

        public void Execute(IServiceCollection serviceCollection, IServiceProvider serviceProvider)
        {
            serviceCollection.AddScoped<ITokenManager, TokenManager>();
            serviceCollection.AddScoped<ITokenLogManager, TokenLogManager>();
        }
    }
}
