﻿using System;
using TL.Engine.SDK.Managers;

namespace TL.Registry.Data.Managers
{
    public interface IRegistryManager : IEntityComparableManager<Entities.Core.Registry, Guid>
    {
    }
}
