﻿using System;
using TL.Engine.SDK.Managers;
using TL.Registry.Data.Entities.Core;

namespace TL.Registry.Data.Managers
{
    public interface IFolderManager : IEntityComparableManager<Folder, Guid>
    {
    }
}
