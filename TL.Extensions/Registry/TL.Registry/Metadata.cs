﻿using TL.Engine.SDK.Modularity;

namespace TL.Registry
{
    public class Metadata : BaseMetadata
    {
        public override string Name => "TL.Registry";
    }
}
