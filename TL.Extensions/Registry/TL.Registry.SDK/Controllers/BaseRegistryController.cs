﻿using Microsoft.AspNetCore.Mvc;
using TL.Engine.SDK.Controllers;

namespace TL.Registry.SDK.Controllers
{
    [Area("Registry")]
    public abstract class BaseRegistryController : BaseController
    {
    }
}
