﻿using TL.Engine.SDK.Modularity;

namespace TL.Registry.SDK
{
    public class Metadata : BaseMetadata
    {
        public override string Name => "TL.Registry.SDK";

        public override string Owner => "TL.Registry";
    }
}
