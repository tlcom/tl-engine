﻿using ExtCore.Data.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace TL.Linker.Data.EntityFramework.Core.Links
{
    public class LinkRegistrar : IEntityRegistrar
    {
        public void RegisterEntities(ModelBuilder modelbuilder)
        {
            modelbuilder.ApplyConfiguration(new LinkConfiguration());
        }
    }
}
