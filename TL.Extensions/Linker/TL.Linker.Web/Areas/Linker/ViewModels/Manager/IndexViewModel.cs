﻿namespace TL.Linker.Web.Areas.Linker.ViewModels.Manager
{
    public class IndexViewModel
    {
        public int Pages { get; set; }

        public int Current { get; set; }

        public int PerPage { get; set; }
    }
}
