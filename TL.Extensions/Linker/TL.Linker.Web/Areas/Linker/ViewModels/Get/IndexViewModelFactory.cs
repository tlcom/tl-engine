﻿namespace TL.Linker.Web.Areas.Linker.ViewModels.Get
{
    public class IndexViewModelFactory
    {
        public IndexViewModel Create()
        {
            return new IndexViewModel() { };
        }
    }
}
