﻿using TL.Engine.SDK.Modularity;

namespace TL.JustBot.SDK
{
    public class Metadata : BaseMetadata
    {
        public override string Name => "TL.JustBot.SDK";

        public override string Owner => "TL.JustBot";
    }
}
