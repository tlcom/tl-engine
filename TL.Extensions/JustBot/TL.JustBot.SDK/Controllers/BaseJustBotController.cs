﻿using Microsoft.AspNetCore.Mvc;
using TL.Engine.SDK.Controllers;

namespace TL.JustBot.SDK.Controllers
{
    [Area("JustBot")]
    public abstract class BaseJustBotController : BaseController
    {
    }
}
