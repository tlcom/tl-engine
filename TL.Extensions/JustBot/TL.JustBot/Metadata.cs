﻿using TL.Engine.SDK.Modularity;

namespace TL.JustBot
{
    public class Metadata : BaseMetadata
    {
        public override string Name => "TL.JustBot";
    }
}
