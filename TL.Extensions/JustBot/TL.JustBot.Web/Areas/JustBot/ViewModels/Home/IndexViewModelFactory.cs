﻿namespace TL.JustBot.Web.Areas.JustBot.ViewModels.Home
{
    public class IndexViewModelFactory
    {
        public IndexViewModel Create()
        {
            return new IndexViewModel() { };
        }
    }
}
