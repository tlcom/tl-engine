﻿using System;
using TL.Engine.SDK.Managers;
using TL.JustBot.Data.Entities.Common;

namespace TL.JustBot.Data.Managers
{
    public interface IHistoryManager : IEntityComparableManager<History, Guid>
    {
    }
}