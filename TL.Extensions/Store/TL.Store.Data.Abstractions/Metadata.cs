﻿using TL.Engine.SDK.Modularity;

namespace TL.Store.Data.Abstractions
{
    public class Metadata : BaseMetadata
    {
        public override string Name => "TL.Store.Data.Abstractions";

        public override string Owner => "TL.Store";

        public override string Description =>
                $"Модуль абстракций данных. Промежуточный слой интерфейсов, описывающих методы работы, допустимые с данными модуля {Owner}.";
    }
}
