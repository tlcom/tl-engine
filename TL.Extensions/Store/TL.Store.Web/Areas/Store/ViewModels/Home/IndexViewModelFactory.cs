﻿namespace TL.Store.Web.Areas.Store.ViewModels.Home
{
    public class IndexViewModelFactory
    {
        public IndexViewModel Create()
        {
            return new IndexViewModel() { };
        }
    }
}
