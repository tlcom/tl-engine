﻿using Microsoft.AspNetCore.Mvc;
using TL.Engine.SDK.Controllers;

namespace TL.Store.SDK.Controllers
{
    [Area("Store")]
    public abstract class BaseStoreController : BaseController
    {
    }
}
