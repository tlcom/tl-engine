﻿using TL.Engine.SDK.Modularity;

namespace TL.Store.SDK
{
    public class Metadata : BaseMetadata
    {
        public override string Name => "TL.Store.SDK";

        public override string Owner => "TL.Store";
    }
}
