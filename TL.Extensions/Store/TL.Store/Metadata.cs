﻿using TL.Engine.SDK.Modularity;

namespace TL.Store
{
    public class Metadata : BaseMetadata
    {
        public override string Name => "TL.Store";
    }
}
