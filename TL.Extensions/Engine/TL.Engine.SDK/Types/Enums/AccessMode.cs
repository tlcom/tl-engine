﻿namespace TL.Engine.SDK.Types.Enums
{
    public enum AccessMode
    {
        Nothing,
        Execute,
        Write,
        ExecuteAndWrite,
        Read,
        ReadAndExecute,
        ReadAndWrite,
        All
    }
}
