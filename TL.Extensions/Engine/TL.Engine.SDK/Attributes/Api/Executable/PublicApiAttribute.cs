﻿namespace TL.Engine.SDK.Attributes.Api.Executable
{
    /// <summary>
    /// Атрибут приватного API-метода.
    /// </summary>
    /// <seealso cref="PrivateApiAttribute" />
    public class PublicApiAttribute : PrivateApiAttribute
    {
    }
}
