﻿using System;
using System.Collections.Generic;
using TL.Engine.Data.Entities.Security;
using TL.Engine.SDK.Repositories;

namespace TL.Engine.Data.Abstractions.Security
{
    public interface IUserRoleRepository : IEntityComparableRepository<UserRole, (Guid, Guid)>
    {
        IEnumerable<UserRole> GetByRole(Role role);

        IEnumerable<UserRole> GetByRoleId(Guid roleId);

        IEnumerable<UserRole> GetByUser(User User);

        IEnumerable<UserRole> GetByUserId(Guid userId);
    }
}
