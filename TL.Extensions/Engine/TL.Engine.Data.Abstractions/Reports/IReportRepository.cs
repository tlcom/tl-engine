﻿using System;
using TL.Engine.Data.Entities.Reports;
using TL.Engine.SDK.Repositories;

namespace TL.Engine.Data.Abstractions.Reports
{
    public interface IReportRepository : IEntityComparableRepository<Report, Guid>
    {
    }
}
