﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TL.Engine.Data.Managers;
using TL.Engine.Web.Areas.Account.ViewModels;

namespace TL.Engine.Web.Areas.Account.Controllers
{
    public class RegisterController : __AccountController__
    {
        public RegisterController(IUserManager userManager) : base(userManager)
        {
        }
        
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Index(string returnUrl = null)
        {
            if (User.Identity.IsAuthenticated)
                return Redirect(Url.Content("/"));

            return View(new RegisterViewModel() { ReturnUrl = Url.IsLocalUrl(returnUrl) ? returnUrl : Url.Content("~/") });
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult Index(RegisterViewModel model)
        {
            if (User.Identity.IsAuthenticated)
                return Redirect(Url.Content("/"));

            if (ModelState.IsValid)
            {
                if (model.Password != model.ConfirmPassword)
                {
                    ModelState.AddModelError("", $"Пароли не совпадают!");
                }
                else
                {
                    var user = UserManager.Get(model.Username);
                    if (user == null)
                    {
                        user = UserManager.Create(model.Username, model.Password);
                        UserManager.Authenticate(user, HttpContext);
                        return Redirect(model.ReturnUrl);
                    }
                    else
                        ModelState.AddModelError("", $"Похоже, что логин {model.Username} уже занят.");
                }
            }
            return View(model);
        }
    }
}
