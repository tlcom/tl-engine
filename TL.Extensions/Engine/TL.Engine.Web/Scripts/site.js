﻿(function () {
    'use strict';
    window.addEventListener('load', function () {
        var forms = document.getElementsByClassName('needs-validation');
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

$(document).ready(function () {
    $('.table-responsive-stack').find("th").each(function (i) {

        $('.table-responsive-stack td:nth-child(' + (i + 1) + ')').prepend('<span class="table-responsive-stack-thead">' + $(this).text() + ':</span> ');
        $('.table-responsive-stack-thead').hide();
    });
    $('.table-responsive-stack').each(function () {
        var thCount = $(this).find("th").length;
        var rowGrow = 100 / thCount + '%';
        $(this).find("th, td").css('flex-basis', rowGrow);
    });
    function flexTable() {
        if ($(window).width() < 768) {
            $(".table-responsive-stack").each(function (i) {
                $(this).find(".table-responsive-stack-thead").show();
            });
        } else {
            $(".table-responsive-stack").each(function (i) {
                $(this).find(".table-responsive-stack-thead").hide();
            });
        }
    }
    flexTable();
    window.onresize = function (event) {
        flexTable();
    };
});

function _alert(f) {
    $.ajax({
        url: '/alert?message=' + f,
        type: 'post',
        success: function (result) {
            $(document).find('#status-messages-block').append(result);
        }
    })
}

function server_alert(f, s, r) {
    if (arguments.length == 1) {
        $.ajax({
            url: f,
            type: 'post',
            success: function (result) {
                $(document).find('#status-messages-block').append(result);
            }
        })
    }
    else if (arguments.length > 1) {
        r = r == null ? false : r;
        $.ajax({
            url: f + '?' + $(document).find(s).serialize(),
            type: 'post',
            success: function (result) {
                $(document).find('#status-messages-block').append(result);
                if (r) {
                    $(document).find(s)[0].reset();
                }
            }
        })
    }
}