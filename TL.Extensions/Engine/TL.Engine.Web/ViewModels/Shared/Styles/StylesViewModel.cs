﻿using System.Collections.Generic;

namespace TL.Engine.Web.ViewModels.Shared
{
    public class StylesViewModel
    {
        public IEnumerable<StyleViewModel> Styles { get; set; }
    }
}