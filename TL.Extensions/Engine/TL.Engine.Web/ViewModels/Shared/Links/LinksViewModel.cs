﻿using System.Collections.Generic;

namespace TL.Engine.Web.ViewModels.Shared
{
    public class LinksViewModel
    {
        public IEnumerable<LinkViewModel> Items { get; set; }
    }
}