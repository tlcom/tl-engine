﻿using System.Collections.Generic;

namespace TL.Engine.Web.ViewModels.Shared
{
    public class ScriptsViewModel
    {
        public IEnumerable<ScriptViewModel> Scripts { get; set; }
    }
}