﻿using ExtCore.Data.Abstractions;
using Microsoft.AspNetCore.Mvc;
using TL.Engine.SDK.Controllers;

namespace TL.Account.SDK.Controllers
{
    [Area("Account")]
    public abstract class BaseAccountController : BaseController
    {
    }
}
