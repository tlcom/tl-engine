﻿namespace TL.Account.Web.Areas.Account.ViewModels.Home
{
    public class IndexViewModelFactory
    {
        public IndexViewModel Create()
        {
            return new IndexViewModel() { };
        }
    }
}
