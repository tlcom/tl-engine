﻿namespace TL.Integrations.Web.Areas.Integrations.ViewModels.Home
{
    public class IndexViewModelFactory
    {
        public IndexViewModel Create()
        {
            return new IndexViewModel() { };
        }
    }
}
