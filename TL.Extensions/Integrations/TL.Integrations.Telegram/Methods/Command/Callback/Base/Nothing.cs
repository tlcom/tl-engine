﻿using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using TL.Engine.SDK.Services;
using TL.Integrations.SDK.Telegram.Handlers;
using TlMessage = TL.Integrations.SDK.Telegram.Messages.Message;

namespace TL.Integrations.Telegram.Methods.Command.Callback.Base
{
    public class Nothing : BaseBotCallbackHCommandMethod
    {
        public Nothing(IActivatorService activator) : base(activator)
        {
        }

        public override string Command => "nothing";

        public override string Description => "";

        public override int Priority => int.MinValue;

        public override bool IsBlocker => false;

        public override bool IsPrivate => true;

        protected override async Task<IHandlerMethodResult> ExecuteAsync(CallbackQuery callbackQuery, params object[] args)
        {
            await Bot.SendAsync(new TlMessage()
            {
                MessageType = MessageType.Text,
                IsCallbackAnswer = true,

                ChatId = callbackQuery.From.Id,
                CallbackQueryId = callbackQuery.Id,
            });

            ContinueExecute();

            return new HandlerMethodResult(true);
        }
    }
}
