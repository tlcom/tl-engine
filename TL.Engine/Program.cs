﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.CommandLineUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using TL.Engine.Services;
using TL.Engine.SDK.Services;

namespace TL.Engine
{
    public class Program
    {
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();

        public static void Main(string[] args)
        {
            var commandLineApplication = new CommandLineApplication(false);
            var doMigrate = commandLineApplication.Option(
                "--ef-migrate",
                "Apply entity framework migrations and exit",
                CommandOptionType.NoValue);

            var verifyMigrate = commandLineApplication.Option(
                "--ef-migrate-check",
                "Check the status of entity framework migrations",
                CommandOptionType.NoValue);

            var startupDuration = commandLineApplication.Option(
                "--startup-duration",
                "Sets the duration of the transition between engine start points",
                CommandOptionType.SingleValue);

            commandLineApplication.HelpOption("-? | -h | --help");
            commandLineApplication.OnExecute(() =>
            {
                ExecuteApp(args, doMigrate, verifyMigrate, startupDuration);
                return 0;
            });
            commandLineApplication.Execute(args);
        }

        private static void ExecuteApp(string[] args, CommandOption doMigrate, CommandOption verifyMigrate, CommandOption startupDuration)
        {
            Console.WriteLine("Loading web host");
            var webHost = CreateWebHostBuilder(args).Build();

            if (verifyMigrate.HasValue() && doMigrate.HasValue())
            {
                Console.WriteLine("ef-migrate and ef-migrate-check are mutually exclusive, select one, and try again");
                Environment.Exit(2);
            }

            if (verifyMigrate.HasValue())
            {
                Console.WriteLine("Validating status of Entity Framework migrations");
                DesignTimeStorageContextFactory.Initialize(webHost.Services);
                using (var context = DesignTimeStorageContextFactory.StorageContext)
                {
                    var pendingMigrations = context.Database.GetPendingMigrations();
                    var migrations = pendingMigrations as IList<string> ?? pendingMigrations.ToList();
                    if (!migrations.Any())
                    {
                        Console.WriteLine("No pending migratons");
                        Environment.Exit(0);
                    }

                    Console.WriteLine("Pending migratons {0}", migrations.Count());
                    foreach (var migration in migrations)
                    {
                        Console.WriteLine($"\t{migration}");
                    }

                    Environment.Exit(3);
                }
            }

            if (doMigrate.HasValue())
            {
                Console.WriteLine("Applyting Entity Framework migrations");
                DesignTimeStorageContextFactory.Initialize(webHost.Services);
                using (var context = DesignTimeStorageContextFactory.StorageContext)
                {
                    context.Database.Migrate();
                    Console.WriteLine("All done, closing app");
                    Environment.Exit(0);
                }
            }

            if (startupDuration.HasValue())
            {
                if (int.TryParse(startupDuration.Value(), out int duration))
                {
                    var startupService = webHost.Services.GetService<IStartupService>();
                    startupService.SetDuration(duration);
                }
            }

            webHost.Run();
        }
    }
}
