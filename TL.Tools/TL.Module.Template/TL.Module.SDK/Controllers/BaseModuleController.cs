﻿using Microsoft.AspNetCore.Mvc;
using TL.Engine.SDK.Controllers;

namespace $safeprojectname$.Controllers
{
    [Area("$saferootprojectname$")]
    public abstract class Base$saferootprojectname$Controller : BaseController
    {
    }
}
