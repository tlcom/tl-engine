﻿namespace $safeprojectname$.Areas.$saferootprojectname$.ViewModels.Home
{
    public class IndexViewModelFactory
    {
        public IndexViewModel Create()
        {
            return new IndexViewModel() { };
        }
    }
}
